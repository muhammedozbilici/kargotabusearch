﻿using System;
/// 
/// <summary>
/// @author http://voidException.weebly.com
/// Use this code at your own risk ;)
/// </summary>
public class TabuList
{
    
	internal int[,] tabuList;
	public TabuList(int numCities)
	{

        tabuList = new int[numCities,numCities];
	}

	public virtual void tabuMove(int city1, int city2)
	{ //tabus the swap operation
		tabuList[city1,city2] += 5;
		tabuList[city2,city1] += 5;

	}
    
	public virtual void decrementTabu()
	{
        
		for (int i = 0; i < Math.Sqrt(tabuList.Length); i++)
		{
            for (int j = 0; j < (tabuList.Length) / 10; j++)
		   {
			tabuList[i,j] -= tabuList[i,j] <= 0?0:1;
		   }
		}
	}

}

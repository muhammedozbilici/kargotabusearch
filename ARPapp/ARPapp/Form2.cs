﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;
using System.Data.SqlClient;
using System.Drawing.Drawing2D;

namespace ARPapp
{
    public partial class Form2 : Form
    {

        public Form2()
        {
            InitializeComponent();
            

        }

        private void Form2_Load(object sender, EventArgs e)
        {
            
        }

        private void Form2_Paint(object sender, PaintEventArgs e)
        {
            #region noktaları al
            List<Point> acGreen = new List<Point>();
            List<Point> acRed = new List<Point>();
            List<String> bestsol = new List<String>();
            int[][] bestsolChar ;


            #region yeşil noktaları al
            using (SqlConnection con = new SqlConnection("Server=localhost;Database= ARPapp;Trusted_Connection=Yes;"))
            {

                SqlDataAdapter addapter2 = new SqlDataAdapter("select *  from depolar", con);


                DataTable dt5 = new DataTable();
                addapter2.Fill(dt5);

                con.Open();
                for (int i = 0; i < dt5.Rows.Count; i++)
                {

                    acGreen.Add(new Point() { X = Convert.ToInt32(dt5.Rows[i][5].ToString()), Y = Convert.ToInt32(dt5.Rows[i][6].ToString()) });

                }
                con.Close();
            }
            #endregion

            #region kırmızı noktaları al
            using (SqlConnection con = new SqlConnection("Server=localhost;Database= ARPapp;Trusted_Connection=Yes;"))
            {

                SqlDataAdapter addapter2 = new SqlDataAdapter("select *  from kargolar", con);


                DataTable dt5 = new DataTable();
                addapter2.Fill(dt5);

                con.Open();
                for (int i = 0; i < dt5.Rows.Count; i++)
                {

                    acRed.Add(new Point() { X = Convert.ToInt32(dt5.Rows[i][5].ToString()), Y = Convert.ToInt32(dt5.Rows[i][6].ToString()) });

                }
                con.Close();
            }
            #endregion

            #endregion

            #region kırmızı noktaları çiz
            PictureBox[] pctred = new PictureBox[acRed.Count];

            Label[] lblred = new Label[acRed.Count];

            for (int i = 0; i < acRed.Count; i++)
            {
                lblred[i] = new Label();

                lblred[i].Location = new Point(acRed[i].X, acRed[i].Y + 40);
                lblred[i].Text = String.Format("{0}", i);


                pctred[i] = new PictureBox();

                pctred[i].Location = new Point(acRed[i].X, acRed[i].Y);
                pctred[i].Image = Properties.Resources.gmapicon1;
                pctred[i].Size = new System.Drawing.Size(37, 34);

            }
            for (int i = 0; i < acRed.Count; i++)
            {
                this.Controls.Add(lblred[i]);

                this.Controls.Add(pctred[i]);
            }
            #endregion

            #region yeşil noktaları çiz

            PictureBox[] pctGreen = new PictureBox[acGreen.Count];
            Label[] lblGreen = new Label[acGreen.Count];


            for (int i = 0; i < acGreen.Count; i++)
            {
                lblGreen[i] = new Label();

                lblGreen[i].Location = new Point(acGreen[i].X, acGreen[i].Y + 40);
                lblGreen[i].Text = String.Format("{0}", i);

                pctGreen[i] = new PictureBox();

                pctGreen[i].Location = new Point(acGreen[i].X, acGreen[i].Y);
                pctGreen[i].Image = Properties.Resources.gmapicon2;
                pctGreen[i].Size = new System.Drawing.Size(20, 34);

            }
            for (int i = 0; i < acGreen.Count; i++)
            {
                this.Controls.Add(lblGreen[i]);
                this.Controls.Add(pctGreen[i]);
            }
            #endregion

            #region okları çiz
            string[][] bestsolChar2;

            using (SqlConnection con = new SqlConnection("Server=localhost;Database= ARPapp;Trusted_Connection=Yes;"))
            {

                SqlDataAdapter addapter2 = new SqlDataAdapter("select top 1*  from rotalar order by rotaId desc", con);


                DataTable dt6 = new DataTable();
                addapter2.Fill(dt6);

                bestsolChar=new int[dt6.Rows.Count][];
                con.Open();
                bestsolChar2 = new string[dt6.Rows.Count][];

                for (int i = 0; i < dt6.Rows.Count; i++)
                {
                    bestsol.Add(dt6.Rows[i][2].ToString());
                    bestsolChar[i] = bestsol[i].Select(x => ((int)x) - '0').ToArray();
                    bestsolChar2[i] = bestsol[i].Split('-');
                    
                }
                con.Close();
            }

            if (acGreen.Count>0&&acRed.Count>0)
            {
                AdjustableArrowCap bigArrow = new AdjustableArrowCap(5, 10);
                Pen p = new Pen(Color.Blue, 1);
                p.CustomEndCap = bigArrow;
                e.Graphics.DrawLine(p, acGreen[0].X, acGreen[0].Y, acRed[Convert.ToInt32(bestsolChar2[0][1]) - 1].X, acRed[Convert.ToInt32(bestsolChar2[0][1]) - 1].Y);


                for (int i = 1; i < bestsolChar2[0].Length - 1; i++)
                {
                    if (i != bestsolChar2[0].Length - 2)
                    {
                        AdjustableArrowCap bigArrow2 = new AdjustableArrowCap(5, 10);
                        Pen p2 = new Pen(Color.Green, 1);
                        p2.CustomEndCap = bigArrow2;
                        e.Graphics.DrawLine(p2, acRed[Convert.ToInt32(bestsolChar2[0][i]) - 1].X, acRed[Convert.ToInt32(bestsolChar2[0][i]) - 1].Y, acRed[Convert.ToInt32(bestsolChar2[0][i + 1]) - 1].X, acRed[Convert.ToInt32(bestsolChar2[0][i + 1]) - 1].Y);

                    }
                    else
                    {
                        AdjustableArrowCap bigArrow2 = new AdjustableArrowCap(5, 10);
                        Pen p2 = new Pen(Color.Black, 1);
                        p2.CustomEndCap = bigArrow2;
                        e.Graphics.DrawLine(p2, acRed[Convert.ToInt32(bestsolChar2[0][i]) - 1].X, acRed[Convert.ToInt32(bestsolChar2[0][i]) - 1].Y, acGreen[0].X, acGreen[0].Y);
                    }

                }
                
            }
            #endregion

            
        }

        private void Form2_FormClosing(object sender, FormClosingEventArgs e)
        {
            //Application.Exit();
        }







    }
}

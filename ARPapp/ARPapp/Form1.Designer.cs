﻿namespace ARPapp
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.Duraklar = new System.Windows.Forms.TabPage();
            this.button9 = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.button7 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.kargoIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.adresDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.depoIdDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.enlem = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.boylam = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.depoId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.kargolarBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.aRPappDataSet1 = new ARPapp.ARPappDataSet1();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.depoIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.depoAdiDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.adresDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.depolarBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.aRPappDataSet = new ARPapp.ARPappDataSet();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.button3 = new System.Windows.Forms.Button();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button12 = new System.Windows.Forms.Button();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.Araclar = new System.Windows.Forms.TabPage();
            this.button11 = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.dataGridView4 = new System.Windows.Forms.DataGridView();
            this.aractipDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.aracIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.kapasiteDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.depoIdDataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ortalamahizDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ortalamayakitDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.emisyonDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.araclarBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.aRPappDataSet3 = new ARPapp.ARPappDataSet3();
            this.button8 = new System.Windows.Forms.Button();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.Sonuçlar = new System.Windows.Forms.TabPage();
            this.label7 = new System.Windows.Forms.Label();
            this.buttonSure = new System.Windows.Forms.Button();
            this.buttonEmis = new System.Windows.Forms.Button();
            this.buttonTL = new System.Windows.Forms.Button();
            this.buttonKM = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.dataGridView5 = new System.Windows.Forms.DataGridView();
            this.rotaIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rotaAdiDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.guzergahDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.initKm = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bestKm = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.initCost = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bestCost = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.initEmisyon = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bestEmisyon = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.initSureDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bestSureDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rotalarBindingSource2 = new System.Windows.Forms.BindingSource(this.components);
            this.aRPappDataSet6 = new ARPapp.ARPappDataSet6();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.button4 = new System.Windows.Forms.Button();
            this.dataGridView3 = new System.Windows.Forms.DataGridView();
            this.rotalarBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.aRPappDataSet5 = new ARPapp.ARPappDataSet5();
            this.araclarBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.aRPappDataSet2 = new ARPapp.ARPappDataSet2();
            this.gmap = new GMap.NET.WindowsForms.GMapControl();
            this.depolarTableAdapter = new ARPapp.ARPappDataSetTableAdapters.depolarTableAdapter();
            this.kargolarTableAdapter = new ARPapp.ARPappDataSet1TableAdapters.kargolarTableAdapter();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.araclarTableAdapter = new ARPapp.ARPappDataSet2TableAdapters.araclarTableAdapter();
            this.araclarTableAdapter1 = new ARPapp.ARPappDataSet3TableAdapters.araclarTableAdapter();
            this.aRPappDataSet4 = new ARPapp.ARPappDataSet4();
            this.rotalarBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.rotalarTableAdapter = new ARPapp.ARPappDataSet4TableAdapters.rotalarTableAdapter();
            this.rotalarTableAdapter1 = new ARPapp.ARPappDataSet5TableAdapters.rotalarTableAdapter();
            this.rotalarTableAdapter2 = new ARPapp.ARPappDataSet6TableAdapters.rotalarTableAdapter();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.tabControl1.SuspendLayout();
            this.Duraklar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.kargolarBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.aRPappDataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.depolarBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.aRPappDataSet)).BeginInit();
            this.flowLayoutPanel3.SuspendLayout();
            this.flowLayoutPanel2.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.Araclar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.araclarBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.aRPappDataSet3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.Sonuçlar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rotalarBindingSource2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.aRPappDataSet6)).BeginInit();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rotalarBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.aRPappDataSet5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.araclarBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.aRPappDataSet2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.aRPappDataSet4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rotalarBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.Duraklar);
            this.tabControl1.Controls.Add(this.Araclar);
            this.tabControl1.Controls.Add(this.Sonuçlar);
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Location = new System.Drawing.Point(4, 71);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(482, 497);
            this.tabControl1.TabIndex = 0;
            // 
            // Duraklar
            // 
            this.Duraklar.Controls.Add(this.button9);
            this.Duraklar.Controls.Add(this.label3);
            this.Duraklar.Controls.Add(this.button7);
            this.Duraklar.Controls.Add(this.label2);
            this.Duraklar.Controls.Add(this.dataGridView2);
            this.Duraklar.Controls.Add(this.dataGridView1);
            this.Duraklar.Controls.Add(this.flowLayoutPanel3);
            this.Duraklar.Controls.Add(this.flowLayoutPanel2);
            this.Duraklar.Controls.Add(this.flowLayoutPanel1);
            this.Duraklar.Location = new System.Drawing.Point(4, 22);
            this.Duraklar.Name = "Duraklar";
            this.Duraklar.Padding = new System.Windows.Forms.Padding(3);
            this.Duraklar.Size = new System.Drawing.Size(474, 471);
            this.Duraklar.TabIndex = 0;
            this.Duraklar.Text = "Duraklar";
            this.Duraklar.UseVisualStyleBackColor = true;
            this.Duraklar.Click += new System.EventHandler(this.gMapControl1_Load);
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(354, 74);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(116, 21);
            this.button9.TabIndex = 9;
            this.button9.Text = "Haritayı Temizle";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 267);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(44, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Depolar";
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(382, 16);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(88, 52);
            this.button7.TabIndex = 8;
            this.button7.Text = "Rota Çiz";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 82);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Kargolar ";
            // 
            // dataGridView2
            // 
            this.dataGridView2.AllowUserToAddRows = false;
            this.dataGridView2.AutoGenerateColumns = false;
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.kargoIdDataGridViewTextBoxColumn,
            this.adresDataGridViewTextBoxColumn1,
            this.depoIdDataGridViewTextBoxColumn1,
            this.enlem,
            this.boylam,
            this.depoId});
            this.dataGridView2.DataSource = this.kargolarBindingSource;
            this.dataGridView2.Location = new System.Drawing.Point(3, 98);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.Size = new System.Drawing.Size(467, 152);
            this.dataGridView2.TabIndex = 5;
            // 
            // kargoIdDataGridViewTextBoxColumn
            // 
            this.kargoIdDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.kargoIdDataGridViewTextBoxColumn.DataPropertyName = "kargoId";
            this.kargoIdDataGridViewTextBoxColumn.HeaderText = "KargoId";
            this.kargoIdDataGridViewTextBoxColumn.Name = "kargoIdDataGridViewTextBoxColumn";
            this.kargoIdDataGridViewTextBoxColumn.ReadOnly = true;
            this.kargoIdDataGridViewTextBoxColumn.Width = 69;
            // 
            // adresDataGridViewTextBoxColumn1
            // 
            this.adresDataGridViewTextBoxColumn1.DataPropertyName = "adres";
            this.adresDataGridViewTextBoxColumn1.HeaderText = "Adres";
            this.adresDataGridViewTextBoxColumn1.Name = "adresDataGridViewTextBoxColumn1";
            this.adresDataGridViewTextBoxColumn1.ReadOnly = true;
            this.adresDataGridViewTextBoxColumn1.Width = 255;
            // 
            // depoIdDataGridViewTextBoxColumn1
            // 
            this.depoIdDataGridViewTextBoxColumn1.DataPropertyName = "depoId";
            this.depoIdDataGridViewTextBoxColumn1.HeaderText = "Depo No";
            this.depoIdDataGridViewTextBoxColumn1.Name = "depoIdDataGridViewTextBoxColumn1";
            this.depoIdDataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // enlem
            // 
            this.enlem.DataPropertyName = "enlem";
            this.enlem.HeaderText = "enlem";
            this.enlem.Name = "enlem";
            this.enlem.Visible = false;
            // 
            // boylam
            // 
            this.boylam.DataPropertyName = "boylam";
            this.boylam.HeaderText = "boylam";
            this.boylam.Name = "boylam";
            this.boylam.Visible = false;
            // 
            // depoId
            // 
            this.depoId.DataPropertyName = "depoId";
            this.depoId.HeaderText = "depoId";
            this.depoId.Name = "depoId";
            this.depoId.Visible = false;
            // 
            // kargolarBindingSource
            // 
            this.kargolarBindingSource.DataMember = "kargolar";
            this.kargolarBindingSource.DataSource = this.aRPappDataSet1;
            // 
            // aRPappDataSet1
            // 
            this.aRPappDataSet1.DataSetName = "ARPappDataSet1";
            this.aRPappDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.depoIdDataGridViewTextBoxColumn,
            this.depoAdiDataGridViewTextBoxColumn,
            this.adresDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.depolarBindingSource;
            this.dataGridView1.Location = new System.Drawing.Point(3, 283);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(470, 225);
            this.dataGridView1.TabIndex = 4;
            // 
            // depoIdDataGridViewTextBoxColumn
            // 
            this.depoIdDataGridViewTextBoxColumn.DataPropertyName = "depoId";
            this.depoIdDataGridViewTextBoxColumn.HeaderText = "Depo No";
            this.depoIdDataGridViewTextBoxColumn.Name = "depoIdDataGridViewTextBoxColumn";
            this.depoIdDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // depoAdiDataGridViewTextBoxColumn
            // 
            this.depoAdiDataGridViewTextBoxColumn.DataPropertyName = "depoAdi";
            this.depoAdiDataGridViewTextBoxColumn.HeaderText = "Depo Adı";
            this.depoAdiDataGridViewTextBoxColumn.Name = "depoAdiDataGridViewTextBoxColumn";
            this.depoAdiDataGridViewTextBoxColumn.ReadOnly = true;
            this.depoAdiDataGridViewTextBoxColumn.Visible = false;
            // 
            // adresDataGridViewTextBoxColumn
            // 
            this.adresDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.adresDataGridViewTextBoxColumn.DataPropertyName = "adres";
            this.adresDataGridViewTextBoxColumn.HeaderText = "Adres";
            this.adresDataGridViewTextBoxColumn.Name = "adresDataGridViewTextBoxColumn";
            this.adresDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // depolarBindingSource
            // 
            this.depolarBindingSource.DataMember = "depolar";
            this.depolarBindingSource.DataSource = this.aRPappDataSet;
            // 
            // aRPappDataSet
            // 
            this.aRPappDataSet.DataSetName = "ARPappDataSet";
            this.aRPappDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.Controls.Add(this.button3);
            this.flowLayoutPanel3.Location = new System.Drawing.Point(0, 49);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(336, 28);
            this.flowLayoutPanel3.TabIndex = 2;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(3, 3);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(84, 23);
            this.button3.TabIndex = 2;
            this.button3.Text = "Seçilenleri Sil";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.Controls.Add(this.button1);
            this.flowLayoutPanel2.Controls.Add(this.button2);
            this.flowLayoutPanel2.Controls.Add(this.button12);
            this.flowLayoutPanel2.Location = new System.Drawing.Point(0, 16);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(336, 29);
            this.flowLayoutPanel2.TabIndex = 1;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(3, 3);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(77, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "Depo Ekle";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(86, 3);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 1;
            this.button2.Text = "Kargo Ekle";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button12
            // 
            this.button12.Location = new System.Drawing.Point(167, 3);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(75, 23);
            this.button12.TabIndex = 10;
            this.button12.Text = "Grafik";
            this.button12.UseVisualStyleBackColor = true;
            this.button12.Click += new System.EventHandler(this.button12_Click);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.label1);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(460, 17);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(90, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Depo ve Kargolar";
            // 
            // Araclar
            // 
            this.Araclar.Controls.Add(this.button11);
            this.Araclar.Controls.Add(this.label6);
            this.Araclar.Controls.Add(this.label5);
            this.Araclar.Controls.Add(this.label4);
            this.Araclar.Controls.Add(this.radioButton3);
            this.Araclar.Controls.Add(this.radioButton2);
            this.Araclar.Controls.Add(this.radioButton1);
            this.Araclar.Controls.Add(this.dataGridView4);
            this.Araclar.Controls.Add(this.button8);
            this.Araclar.Controls.Add(this.pictureBox4);
            this.Araclar.Controls.Add(this.pictureBox3);
            this.Araclar.Controls.Add(this.pictureBox2);
            this.Araclar.Location = new System.Drawing.Point(4, 22);
            this.Araclar.Name = "Araclar";
            this.Araclar.Padding = new System.Windows.Forms.Padding(3);
            this.Araclar.Size = new System.Drawing.Size(474, 471);
            this.Araclar.TabIndex = 2;
            this.Araclar.Text = "Araçlar";
            this.Araclar.UseVisualStyleBackColor = true;
            // 
            // button11
            // 
            this.button11.Location = new System.Drawing.Point(15, 260);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(75, 23);
            this.button11.TabIndex = 9;
            this.button11.Text = "Seçileni Sil";
            this.button11.UseVisualStyleBackColor = true;
            this.button11.Click += new System.EventHandler(this.button11_Click);
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(354, 162);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(118, 70);
            this.label6.TabIndex = 8;
            this.label6.Text = " Kapasite = 2000 kg       Ort. Yakıt = 15 lt /100   Emisyon = 162 g";
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(165, 162);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(118, 70);
            this.label5.TabIndex = 8;
            this.label5.Text = " Kapasite = 700 kg       Ort. Yakıt = 12 lt /100   Emisyon = 144 g";
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(12, 162);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(118, 70);
            this.label4.TabIndex = 8;
            this.label4.Text = " Kapasite = 500 kg         Ort. Yakıt = 10 lt /100   Emisyon = 133 g";
            // 
            // radioButton3
            // 
            this.radioButton3.AutoSize = true;
            this.radioButton3.Location = new System.Drawing.Point(357, 36);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(56, 17);
            this.radioButton3.TabIndex = 4;
            this.radioButton3.TabStop = true;
            this.radioButton3.Text = "Araç 3";
            this.radioButton3.UseVisualStyleBackColor = true;
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(168, 36);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(56, 17);
            this.radioButton2.TabIndex = 3;
            this.radioButton2.TabStop = true;
            this.radioButton2.Text = "Araç 2";
            this.radioButton2.UseVisualStyleBackColor = true;
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Location = new System.Drawing.Point(12, 36);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(56, 17);
            this.radioButton1.TabIndex = 2;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "Araç 1";
            this.radioButton1.UseVisualStyleBackColor = true;
            // 
            // dataGridView4
            // 
            this.dataGridView4.AutoGenerateColumns = false;
            this.dataGridView4.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView4.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.aractipDataGridViewTextBoxColumn,
            this.aracIdDataGridViewTextBoxColumn,
            this.kapasiteDataGridViewTextBoxColumn,
            this.depoIdDataGridViewTextBoxColumn2,
            this.ortalamahizDataGridViewTextBoxColumn,
            this.ortalamayakitDataGridViewTextBoxColumn,
            this.emisyonDataGridViewTextBoxColumn});
            this.dataGridView4.DataSource = this.araclarBindingSource1;
            this.dataGridView4.Location = new System.Drawing.Point(3, 294);
            this.dataGridView4.Name = "dataGridView4";
            this.dataGridView4.Size = new System.Drawing.Size(471, 177);
            this.dataGridView4.TabIndex = 1;
            // 
            // aractipDataGridViewTextBoxColumn
            // 
            this.aractipDataGridViewTextBoxColumn.DataPropertyName = "aractip";
            this.aractipDataGridViewTextBoxColumn.HeaderText = "Araç Tipi";
            this.aractipDataGridViewTextBoxColumn.Name = "aractipDataGridViewTextBoxColumn";
            this.aractipDataGridViewTextBoxColumn.Width = 125;
            // 
            // aracIdDataGridViewTextBoxColumn
            // 
            this.aracIdDataGridViewTextBoxColumn.DataPropertyName = "aracId";
            this.aracIdDataGridViewTextBoxColumn.HeaderText = "Araç No";
            this.aracIdDataGridViewTextBoxColumn.Name = "aracIdDataGridViewTextBoxColumn";
            this.aracIdDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // kapasiteDataGridViewTextBoxColumn
            // 
            this.kapasiteDataGridViewTextBoxColumn.DataPropertyName = "kapasite";
            this.kapasiteDataGridViewTextBoxColumn.HeaderText = "kapasite";
            this.kapasiteDataGridViewTextBoxColumn.Name = "kapasiteDataGridViewTextBoxColumn";
            this.kapasiteDataGridViewTextBoxColumn.Visible = false;
            // 
            // depoIdDataGridViewTextBoxColumn2
            // 
            this.depoIdDataGridViewTextBoxColumn2.DataPropertyName = "depoId";
            this.depoIdDataGridViewTextBoxColumn2.HeaderText = "Depo No";
            this.depoIdDataGridViewTextBoxColumn2.Name = "depoIdDataGridViewTextBoxColumn2";
            // 
            // ortalamahizDataGridViewTextBoxColumn
            // 
            this.ortalamahizDataGridViewTextBoxColumn.DataPropertyName = "ortalamahiz";
            this.ortalamahizDataGridViewTextBoxColumn.HeaderText = "Ortalama Hız";
            this.ortalamahizDataGridViewTextBoxColumn.Name = "ortalamahizDataGridViewTextBoxColumn";
            // 
            // ortalamayakitDataGridViewTextBoxColumn
            // 
            this.ortalamayakitDataGridViewTextBoxColumn.DataPropertyName = "ortalamayakit";
            this.ortalamayakitDataGridViewTextBoxColumn.HeaderText = "ortalamayakit";
            this.ortalamayakitDataGridViewTextBoxColumn.Name = "ortalamayakitDataGridViewTextBoxColumn";
            this.ortalamayakitDataGridViewTextBoxColumn.Visible = false;
            // 
            // emisyonDataGridViewTextBoxColumn
            // 
            this.emisyonDataGridViewTextBoxColumn.DataPropertyName = "emisyon";
            this.emisyonDataGridViewTextBoxColumn.HeaderText = "emisyon";
            this.emisyonDataGridViewTextBoxColumn.Name = "emisyonDataGridViewTextBoxColumn";
            this.emisyonDataGridViewTextBoxColumn.Visible = false;
            // 
            // araclarBindingSource1
            // 
            this.araclarBindingSource1.DataMember = "araclar";
            this.araclarBindingSource1.DataSource = this.aRPappDataSet3;
            // 
            // aRPappDataSet3
            // 
            this.aRPappDataSet3.DataSetName = "ARPappDataSet3";
            this.aRPappDataSet3.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(12, 7);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(75, 23);
            this.button8.TabIndex = 0;
            this.button8.Text = "Araç Ekle";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackgroundImage = global::ARPapp.Properties.Resources.canter;
            this.pictureBox4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox4.Location = new System.Drawing.Point(357, 60);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(117, 85);
            this.pictureBox4.TabIndex = 7;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackgroundImage = global::ARPapp.Properties.Resources.transit;
            this.pictureBox3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox3.Location = new System.Drawing.Point(168, 60);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(130, 85);
            this.pictureBox3.TabIndex = 6;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackgroundImage = global::ARPapp.Properties.Resources.doblo;
            this.pictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox2.Location = new System.Drawing.Point(12, 60);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(118, 85);
            this.pictureBox2.TabIndex = 5;
            this.pictureBox2.TabStop = false;
            // 
            // Sonuçlar
            // 
            this.Sonuçlar.Controls.Add(this.label7);
            this.Sonuçlar.Controls.Add(this.buttonSure);
            this.Sonuçlar.Controls.Add(this.buttonEmis);
            this.Sonuçlar.Controls.Add(this.buttonTL);
            this.Sonuçlar.Controls.Add(this.buttonKM);
            this.Sonuçlar.Controls.Add(this.button10);
            this.Sonuçlar.Controls.Add(this.chart1);
            this.Sonuçlar.Controls.Add(this.dataGridView5);
            this.Sonuçlar.Location = new System.Drawing.Point(4, 22);
            this.Sonuçlar.Name = "Sonuçlar";
            this.Sonuçlar.Padding = new System.Windows.Forms.Padding(3);
            this.Sonuçlar.Size = new System.Drawing.Size(474, 471);
            this.Sonuçlar.TabIndex = 4;
            this.Sonuçlar.Text = "Sonuçlar";
            this.Sonuçlar.UseVisualStyleBackColor = true;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(268, 264);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(0, 13);
            this.label7.TabIndex = 7;
            // 
            // buttonSure
            // 
            this.buttonSure.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.buttonSure.Location = new System.Drawing.Point(7, 325);
            this.buttonSure.Name = "buttonSure";
            this.buttonSure.Size = new System.Drawing.Size(50, 32);
            this.buttonSure.TabIndex = 6;
            this.buttonSure.Text = "DK.";
            this.buttonSure.UseVisualStyleBackColor = true;
            this.buttonSure.Click += new System.EventHandler(this.buttonSure_Click);
            // 
            // buttonEmis
            // 
            this.buttonEmis.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.buttonEmis.Location = new System.Drawing.Point(7, 291);
            this.buttonEmis.Name = "buttonEmis";
            this.buttonEmis.Size = new System.Drawing.Size(50, 28);
            this.buttonEmis.TabIndex = 5;
            this.buttonEmis.Text = "CO₂";
            this.buttonEmis.UseVisualStyleBackColor = true;
            this.buttonEmis.Click += new System.EventHandler(this.buttonEmis_Click);
            // 
            // buttonTL
            // 
            this.buttonTL.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.buttonTL.Location = new System.Drawing.Point(7, 257);
            this.buttonTL.Name = "buttonTL";
            this.buttonTL.Size = new System.Drawing.Size(50, 27);
            this.buttonTL.TabIndex = 4;
            this.buttonTL.Text = "TL";
            this.buttonTL.UseVisualStyleBackColor = true;
            this.buttonTL.Click += new System.EventHandler(this.buttonTL_Click);
            // 
            // buttonKM
            // 
            this.buttonKM.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.buttonKM.Location = new System.Drawing.Point(7, 221);
            this.buttonKM.Name = "buttonKM";
            this.buttonKM.Size = new System.Drawing.Size(50, 29);
            this.buttonKM.TabIndex = 3;
            this.buttonKM.Text = "MT.";
            this.buttonKM.UseVisualStyleBackColor = true;
            this.buttonKM.Click += new System.EventHandler(this.buttonKM_Click);
            // 
            // button10
            // 
            this.button10.Location = new System.Drawing.Point(7, 10);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(75, 23);
            this.button10.TabIndex = 2;
            this.button10.Text = "Seçileni Sil";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // chart1
            // 
            chartArea1.Name = "ChartArea1";
            this.chart1.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.chart1.Legends.Add(legend1);
            this.chart1.Location = new System.Drawing.Point(63, 205);
            this.chart1.Name = "chart1";
            series1.ChartArea = "ChartArea1";
            series1.Legend = "Legend1";
            series1.Name = "Series1";
            this.chart1.Series.Add(series1);
            this.chart1.Size = new System.Drawing.Size(289, 247);
            this.chart1.TabIndex = 1;
            this.chart1.Text = "chart1";
            // 
            // dataGridView5
            // 
            this.dataGridView5.AutoGenerateColumns = false;
            this.dataGridView5.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView5.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.rotaIdDataGridViewTextBoxColumn,
            this.rotaAdiDataGridViewTextBoxColumn,
            this.guzergahDataGridViewTextBoxColumn,
            this.initKm,
            this.bestKm,
            this.initCost,
            this.bestCost,
            this.initEmisyon,
            this.bestEmisyon,
            this.initSureDataGridViewTextBoxColumn,
            this.bestSureDataGridViewTextBoxColumn});
            this.dataGridView5.DataSource = this.rotalarBindingSource2;
            this.dataGridView5.Location = new System.Drawing.Point(0, 39);
            this.dataGridView5.Name = "dataGridView5";
            this.dataGridView5.Size = new System.Drawing.Size(474, 160);
            this.dataGridView5.TabIndex = 0;
            this.dataGridView5.RowHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridView5_RowHeaderMouseClick);
            // 
            // rotaIdDataGridViewTextBoxColumn
            // 
            this.rotaIdDataGridViewTextBoxColumn.DataPropertyName = "rotaId";
            this.rotaIdDataGridViewTextBoxColumn.HeaderText = "Rota No";
            this.rotaIdDataGridViewTextBoxColumn.Name = "rotaIdDataGridViewTextBoxColumn";
            this.rotaIdDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // rotaAdiDataGridViewTextBoxColumn
            // 
            this.rotaAdiDataGridViewTextBoxColumn.DataPropertyName = "rotaAdi";
            this.rotaAdiDataGridViewTextBoxColumn.HeaderText = "Rota Adı";
            this.rotaAdiDataGridViewTextBoxColumn.Name = "rotaAdiDataGridViewTextBoxColumn";
            this.rotaAdiDataGridViewTextBoxColumn.Visible = false;
            // 
            // guzergahDataGridViewTextBoxColumn
            // 
            this.guzergahDataGridViewTextBoxColumn.DataPropertyName = "guzergah";
            this.guzergahDataGridViewTextBoxColumn.HeaderText = "Güzergah";
            this.guzergahDataGridViewTextBoxColumn.Name = "guzergahDataGridViewTextBoxColumn";
            // 
            // initKm
            // 
            this.initKm.DataPropertyName = "initKm";
            this.initKm.HeaderText = "Başlangıç Mt.";
            this.initKm.Name = "initKm";
            // 
            // bestKm
            // 
            this.bestKm.DataPropertyName = "bestKm";
            this.bestKm.HeaderText = "En İyi Mt. ";
            this.bestKm.Name = "bestKm";
            // 
            // initCost
            // 
            this.initCost.DataPropertyName = "initCost";
            this.initCost.HeaderText = "Başlangıç Maliyet";
            this.initCost.Name = "initCost";
            // 
            // bestCost
            // 
            this.bestCost.DataPropertyName = "bestCost";
            this.bestCost.HeaderText = "En İyi Maliyet";
            this.bestCost.Name = "bestCost";
            // 
            // initEmisyon
            // 
            this.initEmisyon.DataPropertyName = "initEmisyon";
            this.initEmisyon.HeaderText = "Başlangıç Emisyon (gr)";
            this.initEmisyon.Name = "initEmisyon";
            // 
            // bestEmisyon
            // 
            this.bestEmisyon.DataPropertyName = "bestEmisyon";
            this.bestEmisyon.HeaderText = "En İyi Emisyon (gr)";
            this.bestEmisyon.Name = "bestEmisyon";
            // 
            // initSureDataGridViewTextBoxColumn
            // 
            this.initSureDataGridViewTextBoxColumn.DataPropertyName = "initSure";
            this.initSureDataGridViewTextBoxColumn.HeaderText = "Başlangıç Süre (dk)";
            this.initSureDataGridViewTextBoxColumn.Name = "initSureDataGridViewTextBoxColumn";
            // 
            // bestSureDataGridViewTextBoxColumn
            // 
            this.bestSureDataGridViewTextBoxColumn.DataPropertyName = "bestSure";
            this.bestSureDataGridViewTextBoxColumn.HeaderText = "En İyi Süre (dk)";
            this.bestSureDataGridViewTextBoxColumn.Name = "bestSureDataGridViewTextBoxColumn";
            // 
            // rotalarBindingSource2
            // 
            this.rotalarBindingSource2.DataMember = "rotalar";
            this.rotalarBindingSource2.DataSource = this.aRPappDataSet6;
            // 
            // aRPappDataSet6
            // 
            this.aRPappDataSet6.DataSetName = "ARPappDataSet6";
            this.aRPappDataSet6.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.button4);
            this.tabPage1.Controls.Add(this.dataGridView3);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(474, 471);
            this.tabPage1.TabIndex = 5;
            this.tabPage1.Text = "Çıktı";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(382, 7);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 23);
            this.button4.TabIndex = 5;
            this.button4.Text = "Excel Rapor";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // dataGridView3
            // 
            this.dataGridView3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView3.Location = new System.Drawing.Point(7, 36);
            this.dataGridView3.Name = "dataGridView3";
            this.dataGridView3.Size = new System.Drawing.Size(463, 178);
            this.dataGridView3.TabIndex = 4;
            // 
            // rotalarBindingSource1
            // 
            this.rotalarBindingSource1.DataMember = "rotalar";
            this.rotalarBindingSource1.DataSource = this.aRPappDataSet5;
            // 
            // aRPappDataSet5
            // 
            this.aRPappDataSet5.DataSetName = "ARPappDataSet5";
            this.aRPappDataSet5.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // araclarBindingSource
            // 
            this.araclarBindingSource.DataMember = "araclar";
            this.araclarBindingSource.DataSource = this.aRPappDataSet2;
            // 
            // aRPappDataSet2
            // 
            this.aRPappDataSet2.DataSetName = "ARPappDataSet2";
            this.aRPappDataSet2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gmap
            // 
            this.gmap.AutoSize = true;
            this.gmap.Bearing = 0F;
            this.gmap.CanDragMap = true;
            this.gmap.GrayScaleMode = false;
            this.gmap.LevelsKeepInMemmory = 5;
            this.gmap.Location = new System.Drawing.Point(492, 8);
            this.gmap.MarkersEnabled = true;
            this.gmap.MaxZoom = 18;
            this.gmap.MinZoom = 2;
            this.gmap.MouseWheelZoomType = GMap.NET.MouseWheelZoomType.MousePositionAndCenter;
            this.gmap.Name = "gmap";
            this.gmap.NegativeMode = false;
            this.gmap.PolygonsEnabled = true;
            this.gmap.RetryLoadTile = 0;
            this.gmap.RoutesEnabled = true;
            this.gmap.ShowTileGridLines = false;
            this.gmap.Size = new System.Drawing.Size(530, 548);
            this.gmap.TabIndex = 4;
            this.gmap.Zoom = 5D;
            this.gmap.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.gmap_MouseDoubleClick);
            this.gmap.MouseMove += new System.Windows.Forms.MouseEventHandler(this.gmap_MouseMove);
            // 
            // depolarTableAdapter
            // 
            this.depolarTableAdapter.ClearBeforeFill = true;
            // 
            // kargolarTableAdapter
            // 
            this.kargolarTableAdapter.ClearBeforeFill = true;
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 500;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // araclarTableAdapter
            // 
            this.araclarTableAdapter.ClearBeforeFill = true;
            // 
            // araclarTableAdapter1
            // 
            this.araclarTableAdapter1.ClearBeforeFill = true;
            // 
            // aRPappDataSet4
            // 
            this.aRPappDataSet4.DataSetName = "ARPappDataSet4";
            this.aRPappDataSet4.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // rotalarBindingSource
            // 
            this.rotalarBindingSource.DataMember = "rotalar";
            this.rotalarBindingSource.DataSource = this.aRPappDataSet4;
            // 
            // rotalarTableAdapter
            // 
            this.rotalarTableAdapter.ClearBeforeFill = true;
            // 
            // rotalarTableAdapter1
            // 
            this.rotalarTableAdapter1.ClearBeforeFill = true;
            // 
            // rotalarTableAdapter2
            // 
            this.rotalarTableAdapter2.ClearBeforeFill = true;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::ARPapp.Properties.Resources.Capture2;
            this.pictureBox1.Location = new System.Drawing.Point(4, 1);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(205, 64);
            this.pictureBox1.TabIndex = 5;
            this.pictureBox1.TabStop = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1039, 568);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.gmap);
            this.Controls.Add(this.tabControl1);
            this.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "BestVRP";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tabControl1.ResumeLayout(false);
            this.Duraklar.ResumeLayout(false);
            this.Duraklar.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.kargolarBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.aRPappDataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.depolarBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.aRPappDataSet)).EndInit();
            this.flowLayoutPanel3.ResumeLayout(false);
            this.flowLayoutPanel2.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            this.Araclar.ResumeLayout(false);
            this.Araclar.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.araclarBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.aRPappDataSet3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.Sonuçlar.ResumeLayout(false);
            this.Sonuçlar.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rotalarBindingSource2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.aRPappDataSet6)).EndInit();
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rotalarBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.aRPappDataSet5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.araclarBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.aRPappDataSet2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.aRPappDataSet4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rotalarBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage Duraklar;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabPage Araclar;
        private System.Windows.Forms.TabPage Sonuçlar;
        private GMap.NET.WindowsForms.GMapControl gmap;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridView dataGridView2;
        private ARPappDataSet aRPappDataSet;
        private System.Windows.Forms.BindingSource depolarBindingSource;
        private ARPappDataSetTableAdapters.depolarTableAdapter depolarTableAdapter;
        private ARPappDataSet1 aRPappDataSet1;
        private System.Windows.Forms.BindingSource kargolarBindingSource;
        private ARPappDataSet1TableAdapters.kargolarTableAdapter kargolarTableAdapter;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.DataGridView dataGridView3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.DataGridViewTextBoxColumn kargoIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn adresDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn depoIdDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn enlem;
        private System.Windows.Forms.DataGridViewTextBoxColumn boylam;
        private System.Windows.Forms.DataGridViewTextBoxColumn depoId;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.DataGridView dataGridView4;
        private ARPappDataSet2 aRPappDataSet2;
        private System.Windows.Forms.BindingSource araclarBindingSource;
        private ARPappDataSet2TableAdapters.araclarTableAdapter araclarTableAdapter;
        private System.Windows.Forms.RadioButton radioButton3;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private ARPappDataSet3 aRPappDataSet3;
        private System.Windows.Forms.BindingSource araclarBindingSource1;
        private ARPappDataSet3TableAdapters.araclarTableAdapter araclarTableAdapter1;
        private System.Windows.Forms.DataGridViewTextBoxColumn aractipDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn aracIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn kapasiteDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn depoIdDataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn ortalamahizDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ortalamayakitDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn emisyonDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridView dataGridView5;
        private ARPappDataSet4 aRPappDataSet4;
        private System.Windows.Forms.BindingSource rotalarBindingSource;
        private ARPappDataSet4TableAdapters.rotalarTableAdapter rotalarTableAdapter;
        private ARPappDataSet5 aRPappDataSet5;
        private System.Windows.Forms.BindingSource rotalarBindingSource1;
        private ARPappDataSet5TableAdapters.rotalarTableAdapter rotalarTableAdapter1;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button11;
        private ARPappDataSet6 aRPappDataSet6;
        private System.Windows.Forms.BindingSource rotalarBindingSource2;
        private ARPappDataSet6TableAdapters.rotalarTableAdapter rotalarTableAdapter2;
        private System.Windows.Forms.DataGridViewTextBoxColumn depoIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn depoAdiDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn adresDataGridViewTextBoxColumn;
        private System.Windows.Forms.Button buttonSure;
        private System.Windows.Forms.Button buttonEmis;
        private System.Windows.Forms.Button buttonTL;
        private System.Windows.Forms.Button buttonKM;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.DataGridViewTextBoxColumn rotaIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn rotaAdiDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn guzergahDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn initKm;
        private System.Windows.Forms.DataGridViewTextBoxColumn bestKm;
        private System.Windows.Forms.DataGridViewTextBoxColumn initCost;
        private System.Windows.Forms.DataGridViewTextBoxColumn bestCost;
        private System.Windows.Forms.DataGridViewTextBoxColumn initEmisyon;
        private System.Windows.Forms.DataGridViewTextBoxColumn bestEmisyon;
        private System.Windows.Forms.DataGridViewTextBoxColumn initSureDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn bestSureDataGridViewTextBoxColumn;
    }
}


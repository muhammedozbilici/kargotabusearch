﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GMap;
using GMap.NET.WindowsForms;
using GMap.NET.WindowsForms.Markers;
using GMap.NET;
using GMap.NET.MapProviders;
using System.Diagnostics;
using System.Data.SqlClient;
using System.Drawing.Imaging;
using System.Windows.Forms.DataVisualization.Charting;
using System.Threading;

namespace ARPapp
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            this.BackColor = System.Drawing.Color.White;
            InitializeComponent();

            //Application.Run(new Form2());

        }

        private void gMapControl1_Load(object sender, EventArgs e)
        {

        }


        private void Form1_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'aRPappDataSet6.rotalar' table. You can move, or remove it, as needed.
            this.rotalarTableAdapter2.Fill(this.aRPappDataSet6.rotalar);
            // TODO: This line of code loads data into the 'aRPappDataSet5.rotalar' table. You can move, or remove it, as needed.
            this.rotalarTableAdapter1.Fill(this.aRPappDataSet5.rotalar);
            // TODO: This line of code loads data into the 'aRPappDataSet4.rotalar' table. You can move, or remove it, as needed.
            this.rotalarTableAdapter.Fill(this.aRPappDataSet4.rotalar);
            // TODO: This line of code loads data into the 'aRPappDataSet3.araclar' table. You can move, or remove it, as needed.
            this.araclarTableAdapter1.Fill(this.aRPappDataSet3.araclar);
            // TODO: This line of code loads data into the 'aRPappDataSet2.araclar' table. You can move, or remove it, as needed.
            this.araclarTableAdapter.Fill(this.aRPappDataSet2.araclar);

            // TODO: This line of code loads data into the 'aRPappDataSet1.kargolar' table. You can move, or remove it, as needed.
            this.kargolarTableAdapter.Fill(this.aRPappDataSet1.kargolar);
            // TODO: This line of code loads data into the 'aRPappDataSet.depolar' table. You can move, or remove it, as needed.
            this.depolarTableAdapter.Fill(this.aRPappDataSet.depolar);

            gmap.MapProvider = GMap.NET.MapProviders.GoogleMapProvider.Instance;
            GMap.NET.GMaps.Instance.Mode = GMap.NET.AccessMode.ServerOnly;

            gmap.Position = new PointLatLng(40.9813257, 28.7259975);
            gmap.MinZoom = 5;
            gmap.MaxZoom = 17;
            gmap.Zoom = 15;
            gmap.CanDragMap = true;
            gmap.DragButton = System.Windows.Forms.MouseButtons.Left;
            gmap.MouseWheelZoomType = GMap.NET.MouseWheelZoomType.MousePositionAndCenter;


        }

        private void gmap_MouseMove(object sender, MouseEventArgs e)
        {
            double X = gmap.FromLocalToLatLng(e.X, e.Y).Lng;
            double Y = gmap.FromLocalToLatLng(e.X, e.Y).Lat;
        }


        private void button1_Click(object sender, EventArgs e)
        {
            if (button1.BackColor == Color.LightBlue)
            {
                button1.BackColor = Color.Transparent;
            }
            else
            {
                button1.BackColor = Color.LightBlue;
                button2.BackColor = Color.Transparent;
            }
        }

        GMapOverlay markersOverlay2;
        GMapOverlay markersOverlay;
        List<GMapOverlay> mrkoverlay = new List<GMapOverlay>();

        private void gmap_MouseDoubleClick(object sender, MouseEventArgs e)
        {

            #region depo Ekle
            if (button1.BackColor == Color.LightBlue)
            {

                double souradnice1 = gmap.FromLocalToLatLng(e.X, e.Y).Lng;
                double souradnice2 = gmap.FromLocalToLatLng(e.X, e.Y).Lat;

                PointLatLng p = new PointLatLng(souradnice2, souradnice1);
                GDirections d;
                GMap.NET.MapProviders.GoogleMapProvider.Instance.GetDirections(out d, p, p, true, true, true, false, true);
              if (d!=null)
              {
                markersOverlay = new GMapOverlay(gmap, "marker");
                mrkoverlay.Add(markersOverlay);
                GMapMarkerGoogleGreen marker = new GMapMarkerGoogleGreen(new PointLatLng(souradnice2, souradnice1));
                markersOverlay.Markers.Add(marker);
                gmap.Overlays.Add(markersOverlay);


                //marker.ToolTip = new GMapToolTip(marker);
                //marker.ToolTipText = marker.Position.ToString();
                //marker.ToolTipMode = MarkerTooltipMode.Always;
                //Brush ToolTipBackColor = new SolidBrush(Color.Transparent);

                //markerForm f = new markerForm();
                //f.Show();
                //f.adresText = d.StartAddress;
                //if (f.IsDisposed==true)
                //{
                //  string bn = markerForm.ty;

                //}
               
                using (SqlConnection con = new SqlConnection("Server=localhost;Database= ARPapp;Trusted_Connection=Yes;"))
                {
                    
                    SqlCommand cmd = new SqlCommand("insert into depolar (depoAdi,adres,enlem,boylam,x,y) values('depo','"+d.StartAddress+"','" + souradnice2 + "','" + souradnice1 + "','" + e.X + "','" + e.Y + "')");
                    cmd.Connection = con;
                    cmd.CommandType = CommandType.Text;
                    con.Open();
                    SqlDataReader dr = cmd.ExecuteReader();

                    if (dr.Read())
                    {
                        string a = dr["depoAdi"].ToString();

                    }
                    con.Close();
                }

            }
              else
              {
                  MessageBox.Show("Eklediğiniz depoya rota çizilemiyor , lütfen tekrar depo ekleyiniz!");
              }
            }
            #endregion

            #region Kargo Ekle
            if (button2.BackColor == Color.LightBlue)
            {


                double souradnice1 = gmap.FromLocalToLatLng(e.X, e.Y).Lng;
                double souradnice2 = gmap.FromLocalToLatLng(e.X, e.Y).Lat;

                PointLatLng p = new PointLatLng(souradnice2, souradnice1);
                GDirections d;
                GMap.NET.MapProviders.GoogleMapProvider.Instance.GetDirections(out d, p, p, true, true, true, false, true);

                if (d!=null)
                {
                    int depoId = 0;

                    using (SqlConnection con = new SqlConnection("Server=localhost;Database= ARPapp;Trusted_Connection=Yes;"))
                    {

                        SqlCommand cmd = new SqlCommand("select top 1 *from depolar order  by depoId desc");


                        cmd.Connection = con;
                        cmd.CommandType = CommandType.Text;
                        con.Open();
                        SqlDataReader dr = cmd.ExecuteReader();

                        if (dr.Read())
                        {
                            depoId = Convert.ToInt32(dr["depoId"].ToString());

                        }
                        con.Close();
                    }

                    if (depoId!=0)
                    {
                        using (SqlConnection con = new SqlConnection("Server=localhost;Database= ARPapp;Trusted_Connection=Yes;"))
                        {

                            SqlCommand cmd = new SqlCommand("insert into kargolar (adres,enlem,boylam,depoId,x,y) values('" + d.StartAddress + "','" + souradnice2 + "','" + souradnice1 + "','" + depoId + "','" + e.X + "','" + e.Y + "')");


                            cmd.Connection = con;
                            cmd.CommandType = CommandType.Text;
                            con.Open();
                            SqlDataReader dr = cmd.ExecuteReader();

                            if (dr.Read())
                            {
                                string a = dr["depoAdi"].ToString();

                            }
                            con.Close();
                        }
                        markersOverlay2 = new GMapOverlay(gmap, "marker");
                        mrkoverlay.Add(markersOverlay2);
                        GMapMarkerGoogleRed marker = new GMapMarkerGoogleRed(new PointLatLng(souradnice2, souradnice1));
                        markersOverlay2.Markers.Add(marker);
                        gmap.Overlays.Add(markersOverlay2);
                        button1.BackColor = Color.Transparent;

                        
                    }
                    else
                    {
                        MessageBox.Show("Lütfen önce depo ekleyiniz");
                    }





                    
                }
                else
                {
                    MessageBox.Show("Eklediğiniz kargoya rota çizilemiyor , lütfen tekrar kargo ekleyiniz!");

                }
            }

            #endregion

        }

        
        private void button2_Click(object sender, EventArgs e)
        {
            if (button2.BackColor == Color.LightBlue)
            {
                button2.BackColor = Color.Transparent;
            }
            else
            {
                button2.BackColor = Color.LightBlue;
                button1.BackColor = Color.Transparent;
            }
        }


        private void timer1_Tick(object sender, EventArgs e)
        {
            using (SqlConnection con = new SqlConnection("Server=localhost;Database= ARPapp;Trusted_Connection=Yes;"))
            {

                SqlCommand cmd = new SqlCommand("select * from kargolar");
                cmd.CommandType = CommandType.Text;

                SqlDataAdapter addapter = new SqlDataAdapter();
                DataTable dt = new DataTable();
                cmd.Connection = con;
                con.Open();

                addapter.SelectCommand = cmd;
                addapter.Fill(dt);
                if (dataGridView2.RowCount != dt.Rows.Count)
                {
                    dataGridView2.DataSource = dt;
                }

                con.Close();
            }

            using (SqlConnection con = new SqlConnection("Server=localhost;Database= ARPapp;Trusted_Connection=Yes;"))
            {

                SqlCommand cmd = new SqlCommand("select * from depolar");
                cmd.CommandType = CommandType.Text;

                SqlDataAdapter addapter = new SqlDataAdapter();
                DataTable dt = new DataTable();
                cmd.Connection = con;
                con.Open();

                addapter.SelectCommand = cmd;
                addapter.Fill(dt);
                if (dataGridView1.RowCount != dt.Rows.Count)
                {
                    dataGridView1.DataSource = dt;
                }
                con.Close();
            }

            using (SqlConnection con = new SqlConnection("Server=localhost;Database= ARPapp;Trusted_Connection=Yes;"))
            {

                SqlCommand cmd = new SqlCommand("select * from araclar");
                cmd.CommandType = CommandType.Text;

                SqlDataAdapter addapter = new SqlDataAdapter();
                DataTable dt = new DataTable();
                cmd.Connection = con;
                con.Open();

                addapter.SelectCommand = cmd;
                addapter.Fill(dt);
                if (dataGridView4.RowCount-1 != dt.Rows.Count)
                {
                    dataGridView4.DataSource = dt;
                }
                con.Close();
            }

            using (SqlConnection con = new SqlConnection("Server=localhost;Database= ARPapp;Trusted_Connection=Yes;"))
            {

                SqlCommand cmd = new SqlCommand("select * from rotalar order by rotaId desc");
                cmd.CommandType = CommandType.Text;

                SqlDataAdapter addapter = new SqlDataAdapter();
                DataTable dt = new DataTable();
                cmd.Connection = con;
                con.Open();

                addapter.SelectCommand = cmd;
                addapter.Fill(dt);
                if (dataGridView5.RowCount - 1 != dt.Rows.Count)
                {
                    dataGridView5.DataSource = dt;
                }
                con.Close();
            }


        }


        public class coordinates : List<PointLatLng>
        {
            public double Latitude { get; set; }
            public double Longitude { get; set; }
        }

        public class finish : List<PointLatLng>
        {
            public double Latitude { get; set; }
            public double Longitude { get; set; }
        }
        List<GMapOverlay> routesOverlay=new List<GMapOverlay>();


        private void button7_Click(object sender, EventArgs e)
        {
            List<coordinates> temp = new List<coordinates>();
            int[][] matrix;
            List<coordinates> ac = new List<coordinates>();
            List<coordinates> ac2 = new List<coordinates>();

            List<finish> finish2 = new List<finish>();


            double z = 0.0;
            double x = 0.0;
            PointLatLng a = new PointLatLng(z, x);

            using (SqlConnection con = new SqlConnection("Server=localhost;Database= ARPapp;Trusted_Connection=Yes;"))
            {

                #region araçları çekme
                List<int> araclist = new List<int>();

                SqlDataAdapter addapter3 = new SqlDataAdapter("select  *  from araclar", con);
                DataTable dt6 = new DataTable();
                addapter3.Fill(dt6);

                for (int i = 0; i < dt6.Rows.Count; i++)
                {

                    araclist.Add(Convert.ToInt32(dt6.Rows[i][6].ToString()));
                }
                #endregion
                if (araclist.Count>2)
                {


                    #region depodan çekilen start noktası
                    SqlCommand cmd2 = new SqlCommand("select top 1 *  from depolar");
                    cmd2.CommandType = CommandType.Text;

                    SqlDataAdapter addapter2 = new SqlDataAdapter("select  top 1 *  from depolar", con);


                    DataTable dt5 = new DataTable();
                    addapter2.Fill(dt5);

                    con.Open();
                    for (int i = 0; i < dt5.Rows.Count; i++)
                    {

                        ac2.Add(new coordinates() { Latitude = Convert.ToDouble(dt5.Rows[i][3].ToString()), Longitude = Convert.ToDouble(dt5.Rows[i][4].ToString()) });

                    }
                    con.Close();
                    #endregion

                    #region kargolardan çekilen
                    SqlCommand cmd = new SqlCommand("select  *  from kargolar");
                    cmd.CommandType = CommandType.Text;

                    SqlDataAdapter addapter = new SqlDataAdapter("select  *  from kargolar", con);


                    DataTable dt4 = new DataTable();
                    addapter.Fill(dt4);

                    con.Open();
                    for (int i = 0; i < dt4.Rows.Count; i++)
                    {

                        ac.Add(new coordinates() { Latitude = Convert.ToDouble(dt4.Rows[i][2].ToString()), Longitude = Convert.ToDouble(dt4.Rows[i][3].ToString()) });

                    }
                    #endregion

                    #region rota çizdirme

                            #region Greedy Algorithm
                    if (ac.Count > 0 && ac2.Count > 0)
                    {

                        matrix = new int[ac.Count + 1][];
                        for (int i = 0; i < ac.Count + 1; i++)
                        {
                            matrix[i] = new int[ac.Count + 1];
                        }
                        List<int> dizi1 = new List<int>();
                        dizi1.Add(0);

                        List<int> dizi2 = new List<int>();
                        for (int i = 0; i < ac.Count; i++)
                        {
                            dizi2.Add(i + 1);
                        }

                        double[] uzaklik = new double[dt4.Rows.Count];

                        for (int i = 0; i < dt4.Rows.Count; i++)
                        {

                            PointLatLng startDepo = new PointLatLng(ac2[0].Latitude, ac2[0].Longitude);
                            PointLatLng finishDepo = new PointLatLng(ac[i].Latitude, ac[i].Longitude);

                            MapRoute route = GMap.NET.MapProviders.GoogleSatelliteMapProvider.Instance.GetRouteBetweenPoints(startDepo, finishDepo, false, false, 15);

                            uzaklik[i] = route.Distance;


                        }
                        double enkucuksayi = uzaklik.Min();
                        int dizidekiEnkucukdeger = 0;
                        for (int i = 0; i < uzaklik.Length; i++)
                        {
                            if (enkucuksayi == uzaklik[i])
                            {
                                dizidekiEnkucukdeger = i;
                            }
                        }
                        dizi1.Add(dizidekiEnkucukdeger + 1);
                        dizi2.RemoveAt(dizidekiEnkucukdeger);






                        for (int i = 0; i <= ac.Count - 1; i++)
                        {
                            double[] uzaklik2 = new double[dizi2.Count];

                            for (int j = 0; j < dizi2.Count; j++)
                            {
                                if (dizi1.Count - 1 != ac.Count)
                                {
                                    PointLatLng startDepo = new PointLatLng(ac[dizi1[dizi1.Count - 1] - 1].Latitude, ac[dizi1[dizi1.Count - 1] - 1].Longitude);
                                    PointLatLng finishDepo = new PointLatLng(ac[dizi2[j] - 1].Latitude, ac[dizi2[j] - 1].Longitude);

                                    MapRoute route = GMap.NET.MapProviders.GoogleSatelliteMapProvider.Instance.GetRouteBetweenPoints(startDepo, finishDepo, false, false, 15);

                                    uzaklik2[j] = route.Distance;

                                }


                            }
                            if (uzaklik2.Length > 0)
                            {
                                double enkucuksayi2 = uzaklik2.Min();
                                int dizidekiEnkucukdeger2 = 0;
                                for (int k = 0; k < uzaklik2.Length; k++)
                                {
                                    if (enkucuksayi2 == uzaklik2[k])
                                    {
                                        dizidekiEnkucukdeger2 = k;
                                    }
                                }

                                dizi1.Add(dizi2[dizidekiEnkucukdeger2]);

                                dizi2.RemoveAt(dizidekiEnkucukdeger2);

                                if (dizi2.Count == 1)
                                {
                                    dizi1.Add(dizi2[dizi2.Count - 1]);

                                }

                            }

                        }
                        
                    #endregion


                            #region depodan-ilk kargo noktasına çizdirme
                            //double[] uzaklik = new double[dt4.Rows.Count];

                            for (int i = 0; i < dt4.Rows.Count; i++)
                            {

                                PointLatLng startDepo = new PointLatLng(ac2[0].Latitude, ac2[0].Longitude);
                                PointLatLng finishDepo = new PointLatLng(ac[i].Latitude, ac[i].Longitude);

                                MapRoute route = GMap.NET.MapProviders.GoogleSatelliteMapProvider.Instance.GetRouteBetweenPoints(startDepo, finishDepo, false, false, 15);
                                matrix[0][i + 1] = Convert.ToInt32(1000 * (route.Distance));
                                matrix[i + 1][0] = matrix[0][i + 1];
                                matrix[0][0] = 0;

                            }


                            #endregion

                            #region kargolar arası çizdirme



                            for (int i = 0; i < ac.Count; i++)
                            {
                                double[] uzaklik2 = new double[ac.Count];

                                for (int j = 0; j < ac.Count; j++)
                                {
                                    if (i > j)
                                    {
                                        PointLatLng startkargo = new PointLatLng(ac[i].Latitude, ac[i].Longitude);
                                        PointLatLng finishkargo = new PointLatLng(ac[j].Latitude, ac[j].Longitude);

                                        MapRoute route3 = GMap.NET.MapProviders.GoogleSatelliteMapProvider.Instance.GetRouteBetweenPoints(startkargo, finishkargo, false, false, 15);

                                        matrix[i + 1][j + 1] = Convert.ToInt32(1000 * (route3.Distance));
                                        matrix[j + 1][i + 1] = matrix[i + 1][j + 1];

                                    }
                                    else if (i == j)
                                    {
                                        matrix[i + 1][j + 1] = 0;

                                    }
                                }

                            }
                            #endregion

                            #region tabu rota çizdirme
                            if (dizi1.Count > 3)
                            {


                                TSPEnvironment tspEnvironment = new TSPEnvironment();

                                tspEnvironment.distances = new int[matrix.Length][];
                                for (int i = 0; i < Convert.ToInt32(matrix.Length); i++)
                                {
                                    tspEnvironment.distances[i] = new int[matrix.Length];
                                }

                                Array.Copy(matrix, tspEnvironment.distances, matrix.Length);


                                int[] curr = new int[matrix.Length + 1];
                                for (int i = 0; i < matrix.Length; i++)
                                {
                                    curr[i] = dizi1[i];
                                    curr[i + 1] = 0;
                                }

                                int[] currSolution = new int[dizi1.Count];
                                Array.Copy(curr, currSolution, curr.Length);


                                int numberOfIterations = 100;
                                int tabuLength = 20;
                                TabuList tabuList = new TabuList(tabuLength);

                                int[] bestSol = new int[currSolution.Length];
                                Array.Copy(currSolution, 0, bestSol, 0, bestSol.Length);
                                int bestCost = tspEnvironment.getObjectiveFunctionValue(bestSol);
                                int bestkm = 0;

                                currSolution = TabuSearch.getBestNeighbour(tabuList, tspEnvironment, currSolution);

                                int initkm = tspEnvironment.getObjectiveFunctionValue(currSolution);


                                for (int i = 0; i < numberOfIterations; i++)
                                {

                                    currSolution = TabuSearch.getBestNeighbour(tabuList, tspEnvironment, currSolution);

                                    int currCost = tspEnvironment.getObjectiveFunctionValue(currSolution);


                                    if (currCost < bestCost)
                                    {
                                        Array.Copy(currSolution, 0, bestSol, 0, bestSol.Length);
                                        bestCost = currCost;
                                    }
                                    else
                                    {
                                        bestkm = bestCost;
                                    }
                                }
                                List<int> dist = new List<int>();
                                List<int> siralama = new List<int>();
                                List<GDirections> directions = new List<GDirections>();
                                GDirections d;


                                PointLatLng startkargo6 = new PointLatLng(ac2[bestSol[0]].Latitude, ac2[bestSol[0]].Longitude);
                                PointLatLng finishkargo6 = new PointLatLng(ac[bestSol[1] - 1].Latitude, ac[bestSol[1] - 1].Longitude);

                                MapRoute route5 = GMap.NET.MapProviders.GoogleMapProvider.Instance.GetRouteBetweenPoints(startkargo6, finishkargo6, false, false, 15);

                                GMap.NET.MapProviders.GoogleMapProvider.Instance.GetDirections(out d, finishkargo6, finishkargo6, true, true, true, false, true);


                                dist.Add(Convert.ToInt32(1000 * route5.Distance));
                                siralama.Add(bestSol[1]);
                                directions.Add(d);


                                GMapRoute r4 = new GMapRoute(route5.Points, "My route");
                                GMapOverlay routesOverlay3 = new GMapOverlay(gmap, "routes");
                                routesOverlay.Add(routesOverlay3);

                                routesOverlay3.Routes.Add(r4);
                                gmap.Overlays.Add(routesOverlay3);

                                r4.Stroke.Width = 4;
                                r4.Stroke.Color = Color.Blue;


                                for (int i = 1; i < bestSol.Length - 1; i++)
                                {
                                    if (i != bestSol.Length - 2)
                                    {
                                        PointLatLng startkargo7 = new PointLatLng(ac[bestSol[i] - 1].Latitude, ac[bestSol[i] - 1].Longitude);
                                        PointLatLng finishkargo7 = new PointLatLng(ac[bestSol[i + 1] - 1].Latitude, ac[bestSol[i + 1] - 1].Longitude);

                                        MapRoute route6 = GMap.NET.MapProviders.GoogleMapProvider.Instance.GetRouteBetweenPoints(startkargo7, finishkargo7, false, false, 15);

                                                 GMap.NET.MapProviders.GoogleMapProvider.Instance.GetDirections(out d, finishkargo7, finishkargo7, true, true, true, false, true);


                                        dist.Add(Convert.ToInt32(1000 * route6.Distance));
                                        siralama.Add(bestSol[i + 1]);
                                        directions.Add(d);


                                        GMapRoute r5 = new GMapRoute(route6.Points, "My route");
                                        GMapOverlay routesOverlay4 = new GMapOverlay(gmap, "routes");
                                        routesOverlay.Add(routesOverlay4);
                                        routesOverlay4.Routes.Add(r5);
                                        gmap.Overlays.Add(routesOverlay4);

                                        r5.Stroke.Width = 4;
                                        r5.Stroke.Color = Color.DarkCyan;

                                    }
                                    else
                                    {
                                        PointLatLng startkargo7 = new PointLatLng(ac[bestSol[i] - 1].Latitude, ac[bestSol[i] - 1].Longitude);
                                        PointLatLng finishkargo7 = new PointLatLng(ac2[0].Latitude, ac2[0].Longitude);

                                        MapRoute route6 = GMap.NET.MapProviders.GoogleMapProvider.Instance.GetRouteBetweenPoints(startkargo7, finishkargo7, false, false, 15);

                                        GMap.NET.MapProviders.GoogleMapProvider.Instance.GetDirections(out d, finishkargo7, finishkargo7, true, true, true, false, true);


                                        dist.Add(Convert.ToInt32(1000 * route6.Distance));
                                        siralama.Add(0);
                                        directions.Add(d);


                                        GMapRoute r5 = new GMapRoute(route6.Points, "My route");
                                        GMapOverlay routesOverlay4 = new GMapOverlay(gmap, "routes");
                                        routesOverlay.Add(routesOverlay4);
                                        routesOverlay4.Routes.Add(r5);
                                        gmap.Overlays.Add(routesOverlay4);

                                        r5.Stroke.Width = 4;
                                        r5.Stroke.Color = Color.Black;

                                    }


                                }
                            #endregion


                            #region değişkenleri tanımlama

                                string best = "";
                                best = best + bestSol[0].ToString();

                                for (int i = 1; i < bestSol.Length; i++)
                                {
                                    best = best + "-" + bestSol[i].ToString();
                                }
                                double initCost = (initkm / 1000) * 50;
                                double bestCost2 = (bestkm / 1000) * 50;
                                double initEmisyon = 0;
                                double bestEmisyon = 0;
                                int initSure = (initkm * 1) / 60;
                                int bestSure = 0;

                                #endregion


                            #region araç tipine göre değerleri belirleme

                                int kargoKapasite = 100 * ac.Count;
                                if (kargoKapasite <= 500)
                                {
                                    MessageBox.Show(" Kargo kapasitesi " + kargoKapasite + " " + "\n" + "Seçilen araç tipi 1");
                                    initEmisyon = (initkm / 1000) * 133;
                                    bestEmisyon = (bestkm / 1000) * 133;

                                }
                                else if (kargoKapasite <= 700)
                                {
                                    MessageBox.Show(" Kargo kapasitesi " + kargoKapasite + " " + "\n" + "Seçilen araç tipi 2");
                                    initEmisyon = (initkm / 1000) * 144;
                                    bestEmisyon = (bestkm / 1000) * 144;

                                }
                                else if (kargoKapasite <= 2000)
                                {
                                    MessageBox.Show(" Kargo kapasitesi " + kargoKapasite + " " + "\n" + "Seçilen araç tipi 3");
                                    initEmisyon = (initkm / 1000) * 162;
                                    bestEmisyon = (bestkm / 1000) * 162;

                                }

                                #endregion


                            #region bestSure hesabı
                                int toplamKm = 0;
                                for (int i = 0; i < dist.Count; i++)
                                {
                                    toplamKm += dist[i];
                                }
                                bestSure = (toplamKm * 1) / 60;

                                #endregion


                            #region veritabanına yazdırma
                                int oran = (initkm - bestkm) / initkm;


                                SqlCommand cmd3 = new SqlCommand("insert into rotalar (rotaAdi,guzergah,initKm,bestKm,initCost,bestCost,initEmisyon,bestEmisyon,initSure,bestSure) values('merkez','" + best + "','" + initkm + "','" + bestkm + "','" + initCost + "','" + bestCost2 + "','" + initEmisyon + "','" + bestEmisyon + "','" + initSure + "','" + bestSure + "')");
                                cmd3.Connection = con;
                                cmd3.CommandType = CommandType.Text;
                                cmd3.ExecuteNonQuery();

                                #endregion

                            #region rotaID çekme
                                string rotaId = "";
                                SqlCommand cmd5 = new SqlCommand("select top 1 * from rotalar order by rotaId desc");
                                cmd5.Connection = con;
                                cmd5.CommandType = CommandType.Text;
                                SqlDataReader dr = cmd5.ExecuteReader();
                                if (dr.Read())
                                {
                                    rotaId = dr["rotaId"].ToString();

                                }
                                con.Close();
                                #endregion

                            #region zaman penceresi düzenleme
                                con.Open();


                                DateTime baslangicSaat = new DateTime(2014, 12, 15, 08, 00, 00);

                                List<string> zamanAralik = new List<string>();
                                List<string> zamanAralik2 = new List<string>();

                                DateTime bitisSaat = new DateTime(2014, 12, 15, 08, 00, 00);

                                for (int i = 0; i < dist.Count; i++)
                                {
                                    bitisSaat = bitisSaat.AddSeconds(dist[i] * 1);

                                    zamanAralik.Add(bitisSaat.Hour.ToString() + ":" + bitisSaat.Minute.ToString() + ":" + bitisSaat.Second.ToString());


                                    bitisSaat = bitisSaat.AddMinutes(15);
                                    zamanAralik2.Add(bitisSaat.Hour.ToString() + ":" + bitisSaat.Minute.ToString() + ":" + bitisSaat.Second.ToString());

                                }

                                for (int i = 0; i < dist.Count; i++)
                                {
                                    string zamanAralik3 = zamanAralik[i] + " - " + zamanAralik2[i];

                                    SqlCommand cmd4 = new SqlCommand("insert into zamanPenceresi (kargoSira,kargoAdres,mesafe,rotaId,zamanAralik) values('" + siralama[i] + "','" + directions[i].StartAddress + "','" + dist[i] + "','" + rotaId + "','" + zamanAralik3 + "')");
                                    cmd4.Connection = con;
                                    cmd4.CommandType = CommandType.Text;
                                    cmd4.ExecuteNonQuery();
                                }


                                SqlDataAdapter addapter4 = new SqlDataAdapter("select kargoAdres,mesafe,zamanAralik  from zamanPenceresi where rotaId=" + rotaId + "", con);
                                DataTable dt2 = new DataTable();
                                addapter4.Fill(dt2);

                                dataGridView3.DataSource = dt2;
                                dataGridView3.Columns[0].HeaderText = "Kargo Adresleri";
                                dataGridView3.Columns[1].HeaderText = "Mesafeler (mt)";
                                dataGridView3.Columns[0].Width = 270;
                                dataGridView3.Columns[1].Width = 60;

                                #endregion


                            }


                            

                            


                        }
                        #endregion
                        
                    con.Close();
                    
                }
                else
                {
                    MessageBox.Show("Lütfen araç ekleyiniz");
                }
            }

        }

        private void button9_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < routesOverlay.Count; i++)
            {
                gmap.Overlays.Remove(routesOverlay[i]);
            }
            for (int i = 0; i < mrkoverlay.Count; i++)
            {
                gmap.Overlays.Remove(mrkoverlay[i]);
                
            }

            using (SqlConnection con = new SqlConnection("Server=localhost;Database= ARPapp;Trusted_Connection=Yes;"))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("delete  kargolar");
                SqlCommand cmd2 = new SqlCommand("delete  depolar");

                cmd.Connection = con;
                cmd2.Connection = con;

                cmd.CommandType = CommandType.Text;
                cmd2.CommandType = CommandType.Text;
                cmd.ExecuteNonQuery();
                cmd2.ExecuteNonQuery();

                con.Close();


            }
        }

        private void button12_Click(object sender, EventArgs e)
        {
            Form2 f = new Form2();
            f.Show();

        }

        private void button8_Click(object sender, EventArgs e)
        {

            #region depo id çekme
            int depoId = 0;
            using (SqlConnection con = new SqlConnection("Server=localhost;Database= ARPapp;Trusted_Connection=Yes;"))
            {

                SqlCommand cmd = new SqlCommand("select top 1 *from depolar order  by depoId desc");


                cmd.Connection = con;
                cmd.CommandType = CommandType.Text;
                con.Open();
                SqlDataReader dr = cmd.ExecuteReader();

                if (dr.Read())
                {
                    depoId = Convert.ToInt32(dr["depoId"].ToString());

                }
                con.Close();
            }
            
            #endregion

            #region araç türleri belirleme
            string aractip = "";
            string kapasite = "";
            string ortalamahiz = "";
            string ortalamayakit = "";
            string emisyon = "";

            if (radioButton1.Checked == true)
            {
                aractip = "1";
                kapasite = "700";
                ortalamahiz="50";
                ortalamayakit="10";
                emisyon = "133";
            }

            if (radioButton2.Checked == true)
            {
                aractip = "2";
                kapasite = "1000";
                ortalamahiz = "50";
                ortalamayakit = "12";
                emisyon = "144";

            }

            if (radioButton3.Checked == true)
            {
                aractip = "3";
                kapasite = "1300";
                ortalamahiz = "50";
                ortalamayakit = "15";
                emisyon = "162";

            }

            #endregion

            if (depoId!=0)
            {
                if (radioButton1.Checked == true || radioButton2.Checked == true || radioButton3.Checked == true)
                {

                    using (SqlConnection con = new SqlConnection("Server=localhost;Database= ARPapp;Trusted_Connection=Yes;"))
                    {
                        SqlCommand cmd = new SqlCommand("insert into araclar (kapasite,depoId,ortalamahiz,ortalamayakit,emisyon,aractip) values('" + kapasite + "','" + depoId + "','" + ortalamahiz + "','" + ortalamayakit + "','" + emisyon + "','" + aractip + "')");
                        cmd.Connection = con;
                        cmd.CommandType = CommandType.Text;
                        con.Open();
                        SqlDataReader dr = cmd.ExecuteReader();

                        con.Close();

                    }
                }
            }
            else
            {
                MessageBox.Show("Lütfen önce depo ekleyiniz!");
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (dataGridView2.SelectedRows.Count>0)
            {
                using (SqlConnection con = new SqlConnection("Server=localhost;Database= ARPapp;Trusted_Connection=Yes;"))
                {
                    SqlCommand cmd = new SqlCommand();

                    foreach (DataGridViewRow item in this.dataGridView2.SelectedRows)
                    {
                        int id = int.Parse(dataGridView2.Rows[item.Index].Cells[0].Value.ToString());
                        cmd.CommandText = "delete  kargolar where kargoId=" + id + "";
                    }

                    cmd.Connection = con;
                    cmd.CommandType = CommandType.Text;
                    con.Open();

                    SqlDataReader dr = cmd.ExecuteReader();

                    con.Close();

                }

 
            }

            if (dataGridView1.SelectedRows.Count > 0)
            {
                using (SqlConnection con = new SqlConnection("Server=localhost;Database= ARPapp;Trusted_Connection=Yes;"))
                {
                    SqlCommand cmd = new SqlCommand();

                    foreach (DataGridViewRow item in this.dataGridView1.SelectedRows)
                    {
                        int id = int.Parse(dataGridView1.Rows[item.Index].Cells[0].Value.ToString());
                        cmd.CommandText = "delete  depolar where depoId=" + id + "";
                    }

                    cmd.Connection = con;
                    cmd.CommandType = CommandType.Text;
                    con.Open();

                    SqlDataReader dr = cmd.ExecuteReader();

                    con.Close();

                }


            }
        }

        private void button10_Click(object sender, EventArgs e)
        {
            if (dataGridView5.SelectedRows.Count > 0)
            {
                using (SqlConnection con = new SqlConnection("Server=localhost;Database= ARPapp;Trusted_Connection=Yes;"))
                {
                    SqlCommand cmd = new SqlCommand();

                    foreach (DataGridViewRow item in this.dataGridView5.SelectedRows)
                    {
                        if (dataGridView5.Rows[item.Index].Cells[0].Value!=null)
                        {
                            int id = int.Parse(dataGridView5.Rows[item.Index].Cells[0].Value.ToString());
                            cmd.CommandText = "delete  rotalar where rotaId=" + id + "";
                            cmd.Connection = con;
                            cmd.CommandType = CommandType.Text;
                            con.Open();

                            SqlDataReader dr = cmd.ExecuteReader();

                            con.Close();

                        }
                    }


                }


            }
        }

        private void button11_Click(object sender, EventArgs e)
        {
            if (dataGridView4.SelectedRows.Count > 0)
            {
                using (SqlConnection con = new SqlConnection("Server=localhost;Database= ARPapp;Trusted_Connection=Yes;"))
                {
                    SqlCommand cmd = new SqlCommand();

                    foreach (DataGridViewRow item in this.dataGridView4.SelectedRows)
                    {
                        if (dataGridView4.Rows[item.Index].Cells[1].Value!=null)
                        {
                            int id = int.Parse(dataGridView4.Rows[item.Index].Cells[1].Value.ToString());
                            cmd.CommandText = "delete  araclar where aracId=" + id + "";
                            cmd.Connection = con;
                            cmd.CommandType = CommandType.Text;
                            con.Open();

                            SqlDataReader dr = cmd.ExecuteReader();

                            con.Close();

                        }
                    }


                }


            }
        }

        public int id = 0;
        private void dataGridView5_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            #region rotayı id çekme
            if (dataGridView5.SelectedRows.Count > 0)
            {

                foreach (DataGridViewRow item in this.dataGridView5.SelectedRows)
                {
                    var a = dataGridView5.SelectedRows[0].Cells[0].Value;

                    if (a is int)
                    {
                        id = int.Parse(dataGridView5.Rows[item.Index].Cells[0].Value.ToString());
                    }
                }

            }

            #endregion



        }

        private void buttonKM_Click(object sender, EventArgs e)
        {
            chart1.Series.Clear();

            buttonKM.BackColor = Color.LightBlue;
            buttonSure.BackColor = Color.Transparent;
            buttonTL.BackColor = Color.Transparent;
            buttonEmis.BackColor = Color.Transparent;

            if (id > 0)
            {

                List<int> ac2 = new List<int>();
                #region rota bilgileri çekme


                using (SqlConnection con = new SqlConnection("Server=localhost;Database= ARPapp;Trusted_Connection=Yes;"))
                {
                    SqlDataAdapter addapter2 = new SqlDataAdapter("select *  from rotalar where rotaId=" + id + " ", con);

                    DataTable dt5 = new DataTable();
                    addapter2.Fill(dt5);

                    con.Open();
                    for (int i = 0; i < dt5.Rows.Count; i++)
                    {
                        ac2.Add(Convert.ToInt32(dt5.Rows[i][3].ToString()));
                        ac2.Add(Convert.ToInt32(dt5.Rows[i][4].ToString()));
                    }
                    con.Close();
                }
                #endregion

                #region rotayı çizdirme
                int yuzde =Convert.ToInt32(100*(Convert.ToDouble(( ac2[0] - ac2[1]))/Convert.ToDouble(ac2[0])));

                string[] seriesArray = { "Initial KM", "Best Solution KM","Optimize Oranı %"+yuzde+"" };
                int[] pointsArray = { ac2[0], ac2[1] };

                chart1.Palette = ChartColorPalette.Chocolate;
                if (chart1.Series.Count > 2)
                {

                }
                
                Series series = chart1.Series.Add(seriesArray[0]);
                series.Points.Add(pointsArray[0]);
                label7.Text = seriesArray[2];
                label7.ForeColor = Color.Chocolate;

                Series series2 = chart1.Series.Add(seriesArray[1]);

                series2.Points.Add(pointsArray[1]);
                chart1.ChartAreas[0].AxisY.Maximum = ac2[0]*2;

                #endregion
            }
        }

        private void buttonTL_Click(object sender, EventArgs e)
        {
            chart1.Series.Clear();

            buttonTL.BackColor = Color.LightBlue;
            buttonSure.BackColor = Color.Transparent;
            buttonEmis.BackColor = Color.Transparent;
            buttonKM.BackColor = Color.Transparent;

            if (id > 0)
            {

                List<int> ac2 = new List<int>();
                #region rota bilgileri çekme


                using (SqlConnection con = new SqlConnection("Server=localhost;Database= ARPapp;Trusted_Connection=Yes;"))
                {
                    SqlDataAdapter addapter2 = new SqlDataAdapter("select *  from rotalar where rotaId=" + id + " ", con);

                    DataTable dt5 = new DataTable();
                    addapter2.Fill(dt5);

                    con.Open();
                    for (int i = 0; i < dt5.Rows.Count; i++)
                    {
                        ac2.Add(Convert.ToInt32(dt5.Rows[i][5].ToString()));
                        ac2.Add(Convert.ToInt32(dt5.Rows[i][6].ToString()));
                    }
                    con.Close();
                }
                #endregion

                #region rotayı çizdirme
                int yuzde = Convert.ToInt32(100 * (Convert.ToDouble((ac2[0] - ac2[1])) / Convert.ToDouble(ac2[0])));

                string[] seriesArray = { "Initial TL", "Best Solution TL", "Optimize Oranı %" + yuzde + "" };
                int[] pointsArray = { ac2[0], ac2[1] };

                chart1.Palette = ChartColorPalette.SeaGreen;
                Series series = chart1.Series.Add(seriesArray[0]);
                series.Points.Add(pointsArray[0]);
                label7.Text = seriesArray[2];
                label7.ForeColor = Color.SeaGreen;

                Series series2 = chart1.Series.Add(seriesArray[1]);

                series2.Points.Add(pointsArray[1]);
                chart1.ChartAreas[0].AxisY.Maximum = ac2[0] * 2;

                #endregion

            }
        }

        private void buttonEmis_Click(object sender, EventArgs e)
        {
            chart1.Series.Clear();

            buttonEmis.BackColor = Color.LightBlue;
            buttonSure.BackColor = Color.Transparent;
            buttonKM.BackColor = Color.Transparent;
            buttonTL.BackColor = Color.Transparent;

            if (id > 0)
            {

                List<int> ac2 = new List<int>();
                #region rota bilgileri çekme


                using (SqlConnection con = new SqlConnection("Server=localhost;Database= ARPapp;Trusted_Connection=Yes;"))
                {
                    SqlDataAdapter addapter2 = new SqlDataAdapter("select *  from rotalar where rotaId=" + id + " ", con);

                    DataTable dt5 = new DataTable();
                    addapter2.Fill(dt5);

                    con.Open();
                    for (int i = 0; i < dt5.Rows.Count; i++)
                    {
                        ac2.Add(Convert.ToInt32(dt5.Rows[i][7].ToString()));
                        ac2.Add(Convert.ToInt32(dt5.Rows[i][8].ToString()));
                    }
                    con.Close();
                }
                #endregion

                #region rotayı çizdirme
                int yuzde = Convert.ToInt32(100 * (Convert.ToDouble((ac2[0] - ac2[1])) / Convert.ToDouble(ac2[0])));

                string[] seriesArray = { "Initial Emisyon", "Best Solution Emisyon", "Optimize Oranı %" + yuzde + "" };
                int[] pointsArray = { ac2[0], ac2[1] };

                chart1.Palette = ChartColorPalette.Pastel;
                Series series = chart1.Series.Add(seriesArray[0]);
                series.Points.Add(pointsArray[0]);
                label7.Text = seriesArray[2];
                label7.ForeColor = Color.Green;

                Series series2 = chart1.Series.Add(seriesArray[1]);

                series2.Points.Add(pointsArray[1]);
                chart1.ChartAreas[0].AxisY.Maximum = ac2[0] * 2;

                #endregion

            }
        }

        private void buttonSure_Click(object sender, EventArgs e)
        {
            chart1.Series.Clear();

            buttonSure.BackColor = Color.LightBlue;
            buttonKM.BackColor = Color.Transparent;
            buttonTL.BackColor = Color.Transparent;
            buttonEmis.BackColor = Color.Transparent;

            if (id > 0)
            {

                List<int> ac2 = new List<int>();
                #region rota bilgileri çekme


                using (SqlConnection con = new SqlConnection("Server=localhost;Database= ARPapp;Trusted_Connection=Yes;"))
                {
                    SqlDataAdapter addapter2 = new SqlDataAdapter("select *  from rotalar where rotaId=" + id + " ", con);

                    DataTable dt5 = new DataTable();
                    addapter2.Fill(dt5);

                    con.Open();
                    for (int i = 0; i < dt5.Rows.Count; i++)
                    {
                        ac2.Add(Convert.ToInt32(dt5.Rows[i][9].ToString()));
                        ac2.Add(Convert.ToInt32(dt5.Rows[i][10].ToString()));

                    }
                    con.Close();
                }
                #endregion

                #region rotayı çizdirme
                int yuzde = Convert.ToInt32(100 * (Convert.ToDouble((ac2[0] - ac2[1])) / Convert.ToDouble(ac2[0])));

                string[] seriesArray = { "Initial Süre", "Best Solution Süre", "Optimize Oranı %" +yuzde + "" };
                int[] pointsArray = { ac2[0], ac2[1] };

                chart1.Palette = ChartColorPalette.Fire;
                Series series = chart1.Series.Add(seriesArray[0]);
                series.Points.Add(pointsArray[0]);
                label7.Text = seriesArray[2];
                label7.ForeColor = Color.Firebrick;

                Series series2 = chart1.Series.Add(seriesArray[1]);

                series2.Points.Add(pointsArray[1]);
                chart1.ChartAreas[0].AxisY.Maximum = ac2[0] * 2;

                #endregion

            }
        }

        private void copyAlltoClipboard()
        {
            dataGridView3.SelectAll();
            DataObject dataObj = dataGridView3.GetClipboardContent();
            if (dataObj != null)
                Clipboard.SetDataObject(dataObj);
        }
        private void button4_Click(object sender, EventArgs e)
        {
            if (dataGridView3.Rows.Count>0)
            {
                copyAlltoClipboard();
                Microsoft.Office.Interop.Excel.Application xlexcel;
                Microsoft.Office.Interop.Excel.Workbook xlWorkBook;
                Microsoft.Office.Interop.Excel.Worksheet xlWorkSheet;
                object misValue = System.Reflection.Missing.Value;
                xlexcel = new Microsoft.Office.Interop.Excel.Application();
                xlexcel.Visible = true;
                xlWorkBook = xlexcel.Workbooks.Add(misValue);
                xlWorkSheet = (Microsoft.Office.Interop.Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
                Microsoft.Office.Interop.Excel.Range CR = (Microsoft.Office.Interop.Excel.Range)xlWorkSheet.Cells[2, 1];
                CR.Select();
                xlWorkSheet.PasteSpecial(CR, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, true);
                xlWorkSheet.Cells[1, 2] = "Kargo Adresleri";
                xlWorkSheet.Cells[1, 3] = "Mesafeler";
                xlWorkSheet.Cells[1, 4] = "Zaman Aralığı";
                
            }
        }


    }
}